$(function() {
	window.onresize = function() {
		setMenuHeight();
		setIframeSize();
	};
	setMenuHeight();
	setIframeSize();
	loadUserResources();
	loadIndexData();
});

var showInfo = function(id) {
	layer.open({
		shade: 0.3,
		type: 2,
		area: ['555px', '80%'],
		fixed: false, //不固定
		maxmin: true,
		title: "个人信息",
		content: 'user_detail.html?id=' + id
	});
}

var logout = function(id) {
	deleteMethod(domain+"session/"+id,function(data){
		if(data.ret==0){
			window.location.reload();
		}else{
			layer.msg(data.msg);
		}
	})
}

var loadIndexData = function() {
	var payload = sessionStorage.getItem("payload");
	payload = JSON.parse(payload);
	if(payload) {
		get(domain + "sys/user/" + payload.uid, function(data) {
			$("#navbar-container").html(template('navbar-temp', data));
		});
	} else {
		window.location.href = "login.html";
	}
}

var loadUserResources = function() {
	var payload = sessionStorage.getItem("payload");
	payload = JSON.parse(payload);
	if(payload) {
		get(domain + "sys/user/" + payload.uid + "/resources", function(data) {
			$("#menu-group").html(template('menu-temp', data));
			$(".sub-menu").click(function() {
				menuCilck(this);
			});
		});
	} else {
		window.location.href = "login.html";
	}

}

var menuCilck = function(menu) {
	var target = $(menu).attr("data-target");
	if(target) {
		$("li[data-targetId='" + target + "']").click();
		return;
	}
	var id = new Date().getTime();
	var data = {
		name: $(menu).text(),
		id: id,
		href: getResource($(menu).attr("data-href"))
	}
	$(menu).attr("data-target", id);
	$("#content-main").append(template("content-temp", data));
	setIframeSize();
	var bar = $(template("status-bar-temp", data));
	$(".status-bar-container").append(bar);
	barCilck(bar[0]);
	bar.click(function() {
		barCilck(this);
	});
	bar.find(".status-bar-close").click(function() {
		barClose($(this).parent()[0])
	});

}

var barClose = function(bar) {
	if($(bar).hasClass("bar-active")) {
		if($(bar).prev()[0]) {
			$(bar).prev().click();
		} else if($(bar).next()[0]) {
			$(bar).next().click();
		}
	}
	var id = $(bar).attr("data-targetid");
	$("a[data-target='" + id + "']").removeAttr("data-target");
	$(bar).remove();
	$("iframe[data-iframeid='" + id + "']").remove();
}

var barCilck = function(bar) {
	$(bar).parent().find(".bar-active").removeClass("bar-active");
	$(".iframe-active").removeClass(".iframe-active");
	$(".iframe-active").hide();
	$(bar).addClass("bar-active")
	var id = $(bar).attr("data-targetid");
	$("iframe[data-iframeid='" + id + "']").addClass("iframe-active");
	$("iframe[data-iframeid='" + id + "']").show();
}

var setMenuHeight = function() {
	var nav = $("#nav-container").height() + 2;
	var wh = $(window).height();
	$(".menu-container").css("height", wh - nav - 2 + 'px')
	//$(".menu-pannel").css("height", wh - nav + 'px')
}

var setIframeSize = function() {
	var nav = $("#nav-container").height() + 2;
	var wh = $(window).height() - 10;
	var bh = $("#status-bar-div").height();
	var mw = $(".menu-container").width();
	var ww = $("#main-container").width();
	$("iframe").css("height", wh - nav - bh + 'px')
	$("iframe").css("width", ww - 2 - mw + "px")
	$(".main-content").css("width", ww - 2 - mw + "px");
}